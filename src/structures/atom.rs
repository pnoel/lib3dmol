use std::ops::Deref;

/// An [`Atom`] is a sub-structure linked to a [`Residue`].
/// It stores the following properties
/// - atom name;
/// - a_type, the type of the Atom according to the [`AtomType`] enum
/// - atom number (atomid);
/// - Coordinates x, y and z
/// - if the atom is a constituant of the backbone of the protein
/// - occupancy
/// - temp_factor
/// - element
/// - charge
///
/// Parameters occupancy, temp_factor, element and charge are often not taken into account in softwares. They are store in an [`Option`] structure: None if missing and Ok if present.
/// If a PDB is written using the write_pdb function with missing elements, they will be replaced with blank spaces.
///
#[derive(Debug)]
pub struct Atom {
    pub name: String,
    pub a_type: AtomType,
    pub number: u64,
    pub coord: [f32; 3],
    pub is_backbone: bool,
    pub occupancy: Option<f32>,
    pub temp_factor: Option<f32>,
    pub element: Option<String>,
    pub charge: Option<String>,
}

impl Atom {
    /// Create a new structure [`Atom`]. An atom have a name, a type, a number and x, y, z coordinates
    /// If the atom name is "C", "CA", "N", "O", "OT1" or "OT2", it will be consider as backbone
    /// This function set to [`None`] the parameters for occupancy, tempFactor, element and charge. If these informations are needed, use the [`Atom`] structure directly or use the [`Atom::new_complete`] function.
    /// This function should not be used for real usage and just for tests
    /// # Examples
    ///
    /// ````
    /// use lib3dmol::structures::atom;
    ///
    /// let hydrogen = atom::Atom::new(String::from("HT1"), atom::AtomType::Hydrogen, 1, [0.0, 0.0, 0.0]);
    /// assert_eq!(hydrogen.occupancy, None);
    ///
    /// ````
    pub fn new(name: String, atom_type: AtomType, number: u64, coord: [f32; 3]) -> Atom {
        let n = name.deref().trim();
        let back = n == "C" || n == "CA" || n == "N" || n == "O" || n == "OT1" || n == "OT2";
        Atom {
            name,
            number,
            coord,
            a_type: atom_type,
            is_backbone: back,
            occupancy: None,
            temp_factor: None,
            element: None,
            charge: None,
        }
    }

    /// Create a new complete structure [`Atom`] with all parameters
    pub fn new_complete(
        name: String,
        number: u64,
        coord: [f32; 3],
        a_type: AtomType,
        occupancy: Option<f32>,
        temp_factor: Option<f32>,
        element: Option<String>,
        charge: Option<String>,
    ) -> Atom {
        let n = name.deref().trim();
        let back = n == "C" || n == "CA" || n == "N" || n == "O" || n == "OT1" || n == "OT2";
        Atom {
            name,
            number,
            coord,
            a_type,
            is_backbone: back,
            occupancy,
            temp_factor,
            element,
            charge,
        }
    }

    /// Get the name of the [`Atom`]
    ///
    pub fn name(&self) -> String {
        self.name.clone()
    }

    /// Compute the distance between 2 [`Atom`]
    ///
    /// # Examples
    ///
    /// ````
    /// use lib3dmol::structures::atom;
    ///
    /// let h1 = atom::Atom::new(String::from("HT1"), atom::AtomType::Hydrogen, 1, [1.0, 5.0, 2.0]);
    /// let h2 = atom::Atom::new(String::from("HT1"), atom::AtomType::Hydrogen, 1, [11.0, 17.0, 5.0]);
    ///
    /// assert_eq!(15.905973, h1.compute_distance(&h2));
    ///
    /// ````
    pub fn compute_distance(&self, a: &Atom) -> f32 {
        ((self.coord[0] - a.coord[0]).powi(2)
            + (self.coord[1] - a.coord[1]).powi(2)
            + (self.coord[2] - a.coord[2]).powi(2))
        .sqrt()
    }

    /// Get the weight of the [`Atom`] in g/mol based on its [`AtomType`]
    ///
    /// # Examples
    ///
    /// ```
    /// use lib3dmol::structures::atom::{Atom, AtomType};
    ///
    /// let hydrogen = Atom::new(String::from("H"), AtomType::Hydrogen, 1, [0., 0., 0.]);
    ///
    /// assert_eq!(1.008, hydrogen.get_weight());
    /// ```
    pub fn get_weight(&self) -> f32 {
        self.a_type.get_weight()
    }

    /// Get the 1 or 2 letters code of the [`Atom`] based on its [`AtomType`]
    ///
    /// # Examples
    ///
    /// ```
    /// use lib3dmol::structures::atom::{Atom, AtomType};
    ///
    /// let alpha_carbon = Atom::new(String::from("CA"), AtomType::Carbon, 1, [0., 0., 0.]);
    ///
    /// assert_eq!("C", alpha_carbon.get_symbol());
    /// ```
    pub fn get_symbol(&self) -> &str {
        self.a_type.get_symbol()
    }

    /// Get the fullname of the [`Atom`] based on its [`AtomType`]
    ///
    /// # Examples
    ///
    /// ```
    /// use lib3dmol::structures::atom::{Atom, AtomType};
    ///
    /// let hydrogen = Atom::new(String::from("HG21"), AtomType::Hydrogen, 1, [0., 0., 0.]);
    ///
    /// assert_eq!("HYDROGEN", hydrogen.get_fullname());
    /// ```
    pub fn get_fullname(&self) -> &str {
        self.a_type.get_fullname()
    }
}

#[derive(Debug, Clone)]
pub enum AtomType {
    Hydrogen,
    Helium,
    Lithium,
    Beryllium,
    Boron,
    Carbon,
    Nitrogen,
    Oxygen,
    Fluorine,
    Neon,
    Sodium,
    Magnesium,
    Aluminum,
    Silicon,
    Phosphorus,
    Sulfur,
    Chlorine,
    Argon,
    Potassium,
    Calcium,
    Scandium,
    Titanium,
    Vanadium,
    Chromium,
    Manganese,
    Iron,
    Cobalt,
    Nickel,
    Copper,
    Zinc,
    Gallium,
    Germanium,
    Arsenic,
    Selenium,
    Bromine,
    Krypton,
    Rubidium,
    Strontium,
    Yttrium,
    Zirconium,
    Niobium,
    Molybdenum,
    Technetium,
    Ruthenium,
    Rhodium,
    Palladium,
    Silver,
    Cadmium,
    Indium,
    Tin,
    Antimony,
    Tellurium,
    Iodine,
    Xenon,
    Cesium,
    Barium,
    Lanthanum,
    Cerium,
    Praseodymium,
    Neodymium,
    Promethium,
    Samarium,
    Europium,
    Gadolinium,
    Terbium,
    Dysprosium,
    Holmium,
    Erbium,
    Thulium,
    Ytterbium,
    Lutetium,
    Hafnium,
    Tantalum,
    Tungsten,
    Rhenium,
    Osmium,
    Iridium,
    Platinum,
    Gold,
    Mercury,
    Thallium,
    Lead,
    Bismuth,
    Polonium,
    Astatine,
    Radon,
    Francium,
    Radium,
    Actinium,
    Thorium,
    Protactinium,
    Uranium,
    Neptunium,
    Plutonium,
    Americium,
    Curium,
    Berkelium,
    Californium,
    Einsteinium,
    Fermium,
    Mendelevium,
    Nobelium,
    Lawrencium,
    Rutherfordium,
    Dubnium,
    Seaborgium,
    Bohrium,
    Hassium,
    Meitnerium,
    Unknown,
}

struct AtomInfo {
    symbol: &'static str,
    mass: f32,
    fullname: &'static str,
}

impl AtomType {
    fn get_symbol(&self) -> &str {
        self.get_info().unwrap().symbol
    }

    fn get_weight(&self) -> f32 {
        self.get_info().unwrap().mass
    }

    fn get_fullname(&self) -> &str {
        self.get_info().unwrap().fullname
    }

    fn get_info(&self) -> Option<AtomInfo> {
        match self {
            AtomType::Hydrogen => Some(AtomInfo {
                symbol: "H",
                mass: 1.008,
                fullname: "HYDROGEN",
            }),
            AtomType::Helium => Some(AtomInfo {
                symbol: "HE",
                mass: 4.003,
                fullname: "HELIUM",
            }),
            AtomType::Lithium => Some(AtomInfo {
                symbol: "LI",
                mass: 6.941,
                fullname: "LITHIUM",
            }),
            AtomType::Beryllium => Some(AtomInfo {
                symbol: "BE",
                mass: 9.012,
                fullname: "BERYLLIUM",
            }),
            AtomType::Boron => Some(AtomInfo {
                symbol: "B",
                mass: 10.811,
                fullname: "BORON",
            }),
            AtomType::Carbon => Some(AtomInfo {
                symbol: "C",
                mass: 12.011,
                fullname: "CARBON",
            }),
            AtomType::Nitrogen => Some(AtomInfo {
                symbol: "N",
                mass: 14.007,
                fullname: "NITROGEN",
            }),
            AtomType::Oxygen => Some(AtomInfo {
                symbol: "O",
                mass: 15.999,
                fullname: "OXYGEN",
            }),
            AtomType::Fluorine => Some(AtomInfo {
                symbol: "F",
                mass: 18.998,
                fullname: "FLUORINE",
            }),
            AtomType::Neon => Some(AtomInfo {
                symbol: "NE",
                mass: 20.180,
                fullname: "NEON",
            }),
            AtomType::Sodium => Some(AtomInfo {
                symbol: "NA",
                mass: 22.990,
                fullname: "SODIUM",
            }),
            AtomType::Magnesium => Some(AtomInfo {
                symbol: "MG",
                mass: 24.305,
                fullname: "MAGNESIUM",
            }),
            AtomType::Aluminum => Some(AtomInfo {
                symbol: "AL",
                mass: 26.982,
                fullname: "ALUMINUM",
            }),
            AtomType::Silicon => Some(AtomInfo {
                symbol: "SI",
                mass: 28.086,
                fullname: "SILICON",
            }),
            AtomType::Phosphorus => Some(AtomInfo {
                symbol: "P",
                mass: 30.974,
                fullname: "PHOSPHORUS",
            }),
            AtomType::Sulfur => Some(AtomInfo {
                symbol: "S",
                mass: 32.065,
                fullname: "SULFUR",
            }),
            AtomType::Chlorine => Some(AtomInfo {
                symbol: "CL",
                mass: 35.453,
                fullname: "CHLORINE",
            }),
            AtomType::Argon => Some(AtomInfo {
                symbol: "AR",
                mass: 39.948,
                fullname: "ARGON",
            }),
            AtomType::Potassium => Some(AtomInfo {
                symbol: "K",
                mass: 39.098,
                fullname: "POTASSIUM",
            }),
            AtomType::Calcium => Some(AtomInfo {
                symbol: "CA",
                mass: 40.078,
                fullname: "CALCIUM",
            }),
            AtomType::Scandium => Some(AtomInfo {
                symbol: "SC",
                mass: 44.956,
                fullname: "SCANDIUM",
            }),
            AtomType::Titanium => Some(AtomInfo {
                symbol: "TI",
                mass: 47.867,
                fullname: "TITANIUM",
            }),
            AtomType::Vanadium => Some(AtomInfo {
                symbol: "V",
                mass: 50.942,
                fullname: "VANADIUM",
            }),
            AtomType::Chromium => Some(AtomInfo {
                symbol: "CR",
                mass: 51.996,
                fullname: "CHROMIUM",
            }),
            AtomType::Manganese => Some(AtomInfo {
                symbol: "MN",
                mass: 54.938,
                fullname: "MANGANESE",
            }),
            AtomType::Iron => Some(AtomInfo {
                symbol: "FE",
                mass: 55.845,
                fullname: "IRON",
            }),
            AtomType::Cobalt => Some(AtomInfo {
                symbol: "CO",
                mass: 58.933,
                fullname: "COBALT",
            }),
            AtomType::Nickel => Some(AtomInfo {
                symbol: "NI",
                mass: 58.693,
                fullname: "NICKEL",
            }),
            AtomType::Copper => Some(AtomInfo {
                symbol: "CU",
                mass: 63.546,
                fullname: "COPPER",
            }),
            AtomType::Zinc => Some(AtomInfo {
                symbol: "ZN",
                mass: 65.390,
                fullname: "ZINC",
            }),
            AtomType::Gallium => Some(AtomInfo {
                symbol: "GA",
                mass: 69.723,
                fullname: "GALLIUM",
            }),
            AtomType::Germanium => Some(AtomInfo {
                symbol: "GE",
                mass: 72.640,
                fullname: "GERMANIUM",
            }),
            AtomType::Arsenic => Some(AtomInfo {
                symbol: "AS",
                mass: 74.922,
                fullname: "ARSENIC",
            }),
            AtomType::Selenium => Some(AtomInfo {
                symbol: "SE",
                mass: 78.960,
                fullname: "SELENIUM",
            }),
            AtomType::Bromine => Some(AtomInfo {
                symbol: "BR",
                mass: 79.904,
                fullname: "BROMINE",
            }),
            AtomType::Krypton => Some(AtomInfo {
                symbol: "KR",
                mass: 83.800,
                fullname: "KRYPTON",
            }),
            AtomType::Rubidium => Some(AtomInfo {
                symbol: "RB",
                mass: 85.468,
                fullname: "RUBIDIUM",
            }),
            AtomType::Strontium => Some(AtomInfo {
                symbol: "SR",
                mass: 87.620,
                fullname: "STRONTIUM",
            }),
            AtomType::Yttrium => Some(AtomInfo {
                symbol: "Y",
                mass: 88.906,
                fullname: "YTTRIUM",
            }),
            AtomType::Zirconium => Some(AtomInfo {
                symbol: "ZR",
                mass: 91.224,
                fullname: "ZIRCONIUM",
            }),
            AtomType::Niobium => Some(AtomInfo {
                symbol: "NB",
                mass: 92.906,
                fullname: "NIOBIUM",
            }),
            AtomType::Molybdenum => Some(AtomInfo {
                symbol: "MO",
                mass: 95.940,
                fullname: "MOLYBDENUM",
            }),
            AtomType::Technetium => Some(AtomInfo {
                symbol: "TC",
                mass: 98.000,
                fullname: "TECHNETIUM",
            }),
            AtomType::Ruthenium => Some(AtomInfo {
                symbol: "RU",
                mass: 101.070,
                fullname: "RUTHENIUM",
            }),
            AtomType::Rhodium => Some(AtomInfo {
                symbol: "RH",
                mass: 102.906,
                fullname: "RHODIUM",
            }),
            AtomType::Palladium => Some(AtomInfo {
                symbol: "PD",
                mass: 106.420,
                fullname: "PALLADIUM",
            }),
            AtomType::Silver => Some(AtomInfo {
                symbol: "AG",
                mass: 107.868,
                fullname: "SILVER",
            }),
            AtomType::Cadmium => Some(AtomInfo {
                symbol: "CD",
                mass: 112.411,
                fullname: "CADMIUM",
            }),
            AtomType::Indium => Some(AtomInfo {
                symbol: "IN",
                mass: 114.818,
                fullname: "INDIUM",
            }),
            AtomType::Tin => Some(AtomInfo {
                symbol: "SN",
                mass: 118.710,
                fullname: "TIN",
            }),
            AtomType::Antimony => Some(AtomInfo {
                symbol: "SB",
                mass: 121.760,
                fullname: "ANTIMONY",
            }),
            AtomType::Tellurium => Some(AtomInfo {
                symbol: "TE",
                mass: 127.600,
                fullname: "TELLURIUM",
            }),
            AtomType::Iodine => Some(AtomInfo {
                symbol: "I",
                mass: 126.905,
                fullname: "IODINE",
            }),
            AtomType::Xenon => Some(AtomInfo {
                symbol: "XE",
                mass: 131.293,
                fullname: "XENON",
            }),
            AtomType::Cesium => Some(AtomInfo {
                symbol: "CS",
                mass: 132.906,
                fullname: "CESIUM",
            }),
            AtomType::Barium => Some(AtomInfo {
                symbol: "BA",
                mass: 137.327,
                fullname: "BARIUM",
            }),
            AtomType::Lanthanum => Some(AtomInfo {
                symbol: "LA",
                mass: 138.906,
                fullname: "LANTHANUM",
            }),
            AtomType::Cerium => Some(AtomInfo {
                symbol: "CE",
                mass: 140.116,
                fullname: "CERIUM",
            }),
            AtomType::Praseodymium => Some(AtomInfo {
                symbol: "PR",
                mass: 140.908,
                fullname: "PRASEODYMIUM",
            }),
            AtomType::Neodymium => Some(AtomInfo {
                symbol: "ND",
                mass: 144.240,
                fullname: "NEODYMIUM",
            }),
            AtomType::Promethium => Some(AtomInfo {
                symbol: "PM",
                mass: 145.000,
                fullname: "PROMETHIUM",
            }),
            AtomType::Samarium => Some(AtomInfo {
                symbol: "SM",
                mass: 150.360,
                fullname: "SAMARIUM",
            }),
            AtomType::Europium => Some(AtomInfo {
                symbol: "EU",
                mass: 151.964,
                fullname: "EUROPIUM",
            }),
            AtomType::Gadolinium => Some(AtomInfo {
                symbol: "GD",
                mass: 157.250,
                fullname: "GADOLINIUM",
            }),
            AtomType::Terbium => Some(AtomInfo {
                symbol: "TB",
                mass: 158.925,
                fullname: "TERBIUM",
            }),
            AtomType::Dysprosium => Some(AtomInfo {
                symbol: "DY",
                mass: 162.500,
                fullname: "DYSPROSIUM",
            }),
            AtomType::Holmium => Some(AtomInfo {
                symbol: "HO",
                mass: 164.930,
                fullname: "HOLMIUM",
            }),
            AtomType::Erbium => Some(AtomInfo {
                symbol: "ER",
                mass: 167.259,
                fullname: "ERBIUM",
            }),
            AtomType::Thulium => Some(AtomInfo {
                symbol: "TM",
                mass: 168.934,
                fullname: "THULIUM",
            }),
            AtomType::Ytterbium => Some(AtomInfo {
                symbol: "YB",
                mass: 173.040,
                fullname: "YTTERBIUM",
            }),
            AtomType::Lutetium => Some(AtomInfo {
                symbol: "LU",
                mass: 174.967,
                fullname: "LUTETIUM",
            }),
            AtomType::Hafnium => Some(AtomInfo {
                symbol: "HF",
                mass: 178.490,
                fullname: "HAFNIUM",
            }),
            AtomType::Tantalum => Some(AtomInfo {
                symbol: "TA",
                mass: 180.948,
                fullname: "TANTALUM",
            }),
            AtomType::Tungsten => Some(AtomInfo {
                symbol: "W",
                mass: 183.840,
                fullname: "TUNGSTEN",
            }),
            AtomType::Rhenium => Some(AtomInfo {
                symbol: "RE",
                mass: 186.207,
                fullname: "RHENIUM",
            }),
            AtomType::Osmium => Some(AtomInfo {
                symbol: "OS",
                mass: 190.230,
                fullname: "OSMIUM",
            }),
            AtomType::Iridium => Some(AtomInfo {
                symbol: "IR",
                mass: 192.217,
                fullname: "IRIDIUM",
            }),
            AtomType::Platinum => Some(AtomInfo {
                symbol: "PT",
                mass: 195.078,
                fullname: "PLATINUM",
            }),
            AtomType::Gold => Some(AtomInfo {
                symbol: "AU",
                mass: 196.967,
                fullname: "GOLD",
            }),
            AtomType::Mercury => Some(AtomInfo {
                symbol: "HG",
                mass: 200.590,
                fullname: "MERCURY",
            }),
            AtomType::Thallium => Some(AtomInfo {
                symbol: "TL",
                mass: 204.383,
                fullname: "THALLIUM",
            }),
            AtomType::Lead => Some(AtomInfo {
                symbol: "PB",
                mass: 207.200,
                fullname: "LEAD",
            }),
            AtomType::Bismuth => Some(AtomInfo {
                symbol: "BI",
                mass: 208.980,
                fullname: "BISMUTH",
            }),
            AtomType::Polonium => Some(AtomInfo {
                symbol: "PO",
                mass: 209.000,
                fullname: "POLONIUM",
            }),
            AtomType::Astatine => Some(AtomInfo {
                symbol: "AT",
                mass: 210.000,
                fullname: "ASTATINE",
            }),
            AtomType::Radon => Some(AtomInfo {
                symbol: "RN",
                mass: 222.000,
                fullname: "RADON",
            }),
            AtomType::Francium => Some(AtomInfo {
                symbol: "FR",
                mass: 223.000,
                fullname: "FRANCIUM",
            }),
            AtomType::Radium => Some(AtomInfo {
                symbol: "RA",
                mass: 226.000,
                fullname: "RADIUM",
            }),
            AtomType::Actinium => Some(AtomInfo {
                symbol: "AC",
                mass: 227.000,
                fullname: "ACTINIUM",
            }),
            AtomType::Thorium => Some(AtomInfo {
                symbol: "TH",
                mass: 232.038,
                fullname: "THORIUM",
            }),
            AtomType::Protactinium => Some(AtomInfo {
                symbol: "PA",
                mass: 231.036,
                fullname: "PROTACTINIUM",
            }),
            AtomType::Uranium => Some(AtomInfo {
                symbol: "U",
                mass: 238.029,
                fullname: "URANIUM",
            }),
            AtomType::Neptunium => Some(AtomInfo {
                symbol: "NP",
                mass: 237.000,
                fullname: "NEPTUNIUM",
            }),
            AtomType::Plutonium => Some(AtomInfo {
                symbol: "PU",
                mass: 244.000,
                fullname: "PLUTONIUM",
            }),
            AtomType::Americium => Some(AtomInfo {
                symbol: "AM",
                mass: 243.000,
                fullname: "AMERICIUM",
            }),
            AtomType::Curium => Some(AtomInfo {
                symbol: "CM",
                mass: 247.000,
                fullname: "CURIUM",
            }),
            AtomType::Berkelium => Some(AtomInfo {
                symbol: "BK",
                mass: 247.000,
                fullname: "BERKELIUM",
            }),
            AtomType::Californium => Some(AtomInfo {
                symbol: "CF",
                mass: 251.000,
                fullname: "CALIFORNIUM",
            }),
            AtomType::Einsteinium => Some(AtomInfo {
                symbol: "ES",
                mass: 252.000,
                fullname: "EINSTEINIUM",
            }),
            AtomType::Fermium => Some(AtomInfo {
                symbol: "FM",
                mass: 257.000,
                fullname: "FERMIUM",
            }),
            AtomType::Mendelevium => Some(AtomInfo {
                symbol: "MD",
                mass: 258.000,
                fullname: "MENDELEVIUM",
            }),
            AtomType::Nobelium => Some(AtomInfo {
                symbol: "NO",
                mass: 259.000,
                fullname: "NOBELIUM",
            }),
            AtomType::Lawrencium => Some(AtomInfo {
                symbol: "LR",
                mass: 262.000,
                fullname: "LAWRENCIUM",
            }),
            AtomType::Rutherfordium => Some(AtomInfo {
                symbol: "RF",
                mass: 261.000,
                fullname: "RUTHERFORDIUM",
            }),
            AtomType::Dubnium => Some(AtomInfo {
                symbol: "DB",
                mass: 262.000,
                fullname: "DUBNIUM",
            }),
            AtomType::Seaborgium => Some(AtomInfo {
                symbol: "SG",
                mass: 266.000,
                fullname: "SEABORGIUM",
            }),
            AtomType::Bohrium => Some(AtomInfo {
                symbol: "BH",
                mass: 264.000,
                fullname: "BOHRIUM",
            }),
            AtomType::Hassium => Some(AtomInfo {
                symbol: "HS",
                mass: 277.000,
                fullname: "HASSIUM",
            }),
            AtomType::Meitnerium => Some(AtomInfo {
                symbol: "MT",
                mass: 268.000,
                fullname: "MEITNERIUM",
            }),
            AtomType::Unknown => None,
        }
    }
}
