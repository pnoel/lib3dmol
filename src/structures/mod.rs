pub mod atom;
pub mod chain;
pub mod residue;
mod selection_atom;
pub mod structure;

pub trait GetAtom {
    fn get_atom(&self) -> Vec<&atom::Atom>;
    fn compute_weight(&self) -> f32;
}

pub use atom::Atom;
pub use chain::Chain;
pub use residue::Residue;
pub use structure::Structure;
