use super::residue::Residue;
use super::*;
use std::collections::HashMap;

/// A [`Chain`] is a sub-structure linked to a [`Protein`].
/// It contain one or more [`Residue`] and a name
///
#[derive(Debug)]
pub struct Chain {
    pub name: char,
    pub lst_res: Vec<Residue>,
    pub chain_type: ChainTypes,
}

impl Chain {
    /// Create a new [`Chain`] structure with an empty list of residue
    ///
    /// # Examples
    ///
    /// ````
    /// use lib3dmol::structures::chain;
    ///
    /// let my_chain = chain::Chain::new('a', chain::ChainTypes::Protein);
    ///
    /// ````
    pub fn new(name: char, t: ChainTypes) -> Chain {
        Chain {
            name,
            lst_res: Vec::new(),
            chain_type: t,
        }
    }

    /// Add a new structure [`Residue`] to the [`Chain`]
    ///
    /// # Examples
    ///
    /// ````
    /// use lib3dmol::structures::{chain, residue};
    ///
    /// let mut my_chain = chain::Chain::new('a', chain::ChainTypes::Protein);
    /// let lys = residue::Residue::new(String::from("lysine"), 1);
    ///
    /// my_chain.add_res(lys);
    ///
    /// assert_eq!(1, my_chain.get_number_residue());
    ///
    /// ````
    pub fn add_res(&mut self, r: Residue) {
        self.lst_res.push(r);
    }

    /// Get the number of [`Residue`] in the [`Chain`]
    ///
    /// # Examples
    ///
    /// ````
    /// use lib3dmol::structures::chain;
    ///
    /// let my_chain = chain::Chain::new('a', chain::ChainTypes::Protein);
    ///
    /// assert_eq!(0, my_chain.get_number_residue());
    ///
    /// ````
    pub fn get_number_residue(&self) -> u64 {
        self.lst_res.len() as u64
    }

    /// Return a mutable reference of a [`Residue`] with its name. Return None if the
    /// residue does not exist. The method search residue with its index and the icode. If
    /// no icode is necessary, use `None`.
    ///
    /// # Examples
    ///
    /// ````
    /// use lib3dmol::structures::{chain, residue};
    ///
    /// let mut my_chain = chain::Chain::new('a', chain::ChainTypes::Protein);
    /// let lys = residue::Residue::new(String::from("lysine"), 1);
    /// my_chain.add_res(lys);
    ///
    /// assert_eq!(1, my_chain.lst_res[0].res_num);
    /// {
    ///     let mut res_ref = my_chain.get_residue_ref(1, None).unwrap();
    ///     res_ref.res_num = 4;
    /// }
    /// assert_eq!(4, my_chain.lst_res[0].res_num);
    ///
    /// ````
    pub fn get_residue_ref(&mut self, residue_id: u64, residue_icode: Option<char>) -> Option<&mut Residue> {
        for res in &mut self.lst_res {
            if res.res_num == residue_id && res.res_icode == residue_icode {
                return Some(res);
            }
        }
        None
    }

    /// Get the name of the Chain
    pub fn get_name(&self) -> char {
        self.name
    }
}

/// Enumerate to check the types of the parsed atom.
///
/// [`Atom`] will be class into the following map:  
///        "ARG" => ChainTypes::Protein  
///        "LYS" => ChainTypes::Protein  
///        "ASN" => ChainTypes::Protein  
///        "ASP" => ChainTypes::Protein  
///        "GLU" => ChainTypes::Protein  
///        "SER" => ChainTypes::Protein  
///        "THR" => ChainTypes::Protein  
///        "GLN" => ChainTypes::Protein  
///        "CYS" => ChainTypes::Protein  
///        "HIS" => ChainTypes::Protein  
///        "HSD" => ChainTypes::Protein  
///        "HSP" => ChainTypes::Protein  
///        "HSD" => ChainTypes::Protein  
///        "SEC" => ChainTypes::Protein  
///        "GLY" => ChainTypes::Protein  
///        "PRO" => ChainTypes::Protein  
///        "ALA" => ChainTypes::Protein  
///        "VAL" => ChainTypes::Protein  
///        "ILE" => ChainTypes::Protein  
///        "LEU" => ChainTypes::Protein  
///        "MET" => ChainTypes::Protein  
///        "PHE" => ChainTypes::Protein  
///        "TYR" => ChainTypes::Protein  
///        "TRP" => ChainTypes::Protein  
///        "ADE" => ChainTypes::NucleicAcid  
///        "GUA" => ChainTypes::NucleicAcid  
///        "THY" => ChainTypes::NucleicAcid  
///        "CYT" => ChainTypes::NucleicAcid  
///        "TIP3W" => ChainTypes::Wat  
///        "POPC" => ChainTypes::Lipid  
///        "POPE" => ChainTypes::Lipid
///        "HOH" => ChainTypes::Water
///
#[derive(PartialEq, Debug, Copy, Clone)]
pub enum ChainTypes {
    Protein,
    NucleicAcid,
    Lipid,
    Water,
    Unknown,
}

impl ChainTypes {
    /// Return a [`ChainTypes`] according to the "residue" of the atom
    ///
    /// ````
    /// use lib3dmol::structures::chain;
    ///
    /// assert_eq!(chain::ChainTypes::Protein, chain::ChainTypes::get("trp"));
    /// assert_eq!(chain::ChainTypes::Lipid, chain::ChainTypes::get("POPC"));
    /// assert_eq!(chain::ChainTypes::Unknown, chain::ChainTypes::get("toto"));
    ///
    /// ````
    pub fn get(atom: &str) -> ChainTypes {
        match ATOM_TYPES.get(&atom.to_uppercase()[..]) {
            Some(x) => *x,
            None => ChainTypes::Unknown,
        }
    }
}

impl GetAtom for Chain {
    fn get_atom(&self) -> Vec<&atom::Atom> {
        let mut lst_atom: Vec<&atom::Atom> = Vec::new();
        for res in &self.lst_res {
            for atom in &res.lst_atom {
                lst_atom.push(&atom)
            }
        }
        lst_atom
    }
    fn compute_weight(&self) -> f32 {
        self.get_atom().iter().map(|x| x.get_weight()).sum()
    }
}

lazy_static! {
    static ref ATOM_TYPES: HashMap<&'static str, ChainTypes> = [
        ("ARG", ChainTypes::Protein),
        ("LYS", ChainTypes::Protein),
        ("ASN", ChainTypes::Protein),
        ("ASP", ChainTypes::Protein),
        ("GLU", ChainTypes::Protein),
        ("SER", ChainTypes::Protein),
        ("THR", ChainTypes::Protein),
        ("GLN", ChainTypes::Protein),
        ("CYS", ChainTypes::Protein),
        ("HIS", ChainTypes::Protein),
        ("HSD", ChainTypes::Protein),
        ("HSP", ChainTypes::Protein),
        ("HSD", ChainTypes::Protein),
        ("SEC", ChainTypes::Protein),
        ("GLY", ChainTypes::Protein),
        ("PRO", ChainTypes::Protein),
        ("ALA", ChainTypes::Protein),
        ("VAL", ChainTypes::Protein),
        ("ILE", ChainTypes::Protein),
        ("LEU", ChainTypes::Protein),
        ("MET", ChainTypes::Protein),
        ("PHE", ChainTypes::Protein),
        ("TYR", ChainTypes::Protein),
        ("TRP", ChainTypes::Protein),
        ("ADE", ChainTypes::NucleicAcid),
        ("GUA", ChainTypes::NucleicAcid),
        ("THY", ChainTypes::NucleicAcid),
        ("CYT", ChainTypes::NucleicAcid),
        ("TIP3W", ChainTypes::Water),
        ("POPC", ChainTypes::Lipid),
        ("POPE", ChainTypes::Lipid),
        ("HOH", ChainTypes::Water),
    ]
    .iter()
    .cloned()
    .collect();
}
