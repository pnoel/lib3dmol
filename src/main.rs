use lib3dmol::parser;
use lib3dmol::structures::GetAtom;

fn main() {
    let my_structure = parser::read_pdb("tests/tests_file/f2.pdb", "Protein f2");

    println!(
        "Structure name: {}
Number of chain: {}
Number of residue: {}
Number of atom: {}",
        my_structure.name,
        my_structure.get_chain_number(),
        my_structure.get_residue_number(),
        my_structure.get_atom_number()
    );

    // Now we will extract the backbone

    let backbone = my_structure.select_atoms("backbone").unwrap();

    println!(
        "Number of chain: {}
Number of residue: {}
Number of atom: {}",
        backbone.get_chain_number(),
        backbone.get_residue_number(),
        backbone.get_atom_number()
    );
    for atom in backbone.get_atom() {
        println!("{} {}", atom.number, atom.name)
    }
}
